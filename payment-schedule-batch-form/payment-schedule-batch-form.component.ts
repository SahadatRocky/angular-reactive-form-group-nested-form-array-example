import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges
} from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { Observable, debounceTime, map, startWith } from 'rxjs';
import { card_css, form_grid_css, form_grid_span_css, heading_css, label_css, table_header_css } from 'src/app/report/core/css_contstants';
import { MessageService } from 'src/app/shared/services/message.service';
import { PaymentStatus } from '../models/payment-status.model';
import { CashbookType } from '../models/cashbook-type.model';
import { FundSource } from '../models/fundsource.model';
import { PaymentSchedule } from '../models/payment-schedule.model';
import { FinanceAccount } from 'src/app/report/models/finance-account.model';
import { FinancialCode } from '../models/financial-code.model';
import { PaymentMethod } from '../models/payment-method.model';


@Component({
  selector: 'app-payment-schedule-batch-form',
  templateUrl: './payment-schedule-batch-form.component.html',
  styleUrls: ['./payment-schedule-batch-form.component.css']
})
export class PaymentScheduleBatchFormComponent implements OnInit, OnChanges{
  cardCss = card_css;
  headingCss = heading_css;
  labelCss = label_css;
  tableHeaderCss = table_header_css;
  formGridCss = form_grid_css;
  formGridSpanCss = form_grid_span_css;
  toggleChecker: boolean = false;
  filteredStatusList: any[] = [];
  initDone = false;
  form!: FormGroup;
  paymentStatusList!: PaymentStatus[];
  paymentlistBatchFormData !: any[];
  
  @Output() onSubmit = new EventEmitter<PaymentSchedule>();
  @Output() selectedFundFouceIdEmiter = new EventEmitter<string>();
  
  cashbookTypeList !: CashbookType[];
  filteredCashbookTypeList!: Observable<CashbookType[]>;

  @Input()
  set setCashbookTypes(cashbookType: CashbookType[]){
      this.cashbookTypeList = cashbookType;
      if (this.cashbookTypeList?.length > 0) {
        //console.log('cashbookTypes::',this.cashbookTypes);
        // this.cashbookType?.setValidators([
        //   Validators.required,
        //   autocompleteValidator(this.cashbookTypes),
        // ]);
        this.setCashbookTypeList();
      }
  }

  fundSourceTypeList !: FundSource[];
  filteredFundSourceTypeList !: Observable<FundSource[]>;

  @Input()
  set setFundSourceTypes(fundSourceType : FundSource[]){
    this.fundSourceTypeList = fundSourceType;  
    if(this.fundSourceTypeList?.length > 0){
      this.setFundSourceTypeList();
    }
  }
  
  
  financeAccountList !: FinanceAccount[];
  filteredFinancialAccountList !: Observable<FinanceAccount[]>;

  @Input()
  set setFinanceAccountList(financeAccountList: FinanceAccount[]){
    this.financeAccountList = financeAccountList;  
    if(this.financeAccountList.length > 0){
      //  this.payerId?.setValidators([
      //   Validators.required,
      //   autocompleteValidator(this.financeAccountList)
      //  ]);
       this.setFinanceAccountTypeList();
    }
  }

  setFinanceAccountTypeList(){
    if(this.payerId){
      this.filteredFinancialAccountList = this.payerId.valueChanges
      .pipe(
        startWith(''),
        debounceTime(200),
        map((value) => 
          this.financeAccountList.filter((option:any)=> 
            option?.value?.toLowerCase().includes(value.toString().toLowerCase())
          ) ?? ''
        )
      )
    }
}


financialCodeList!: FinancialCode[];
filteredFinancialCodeList!: Observable<FinancialCode[]>;

@Input()
set setFinancialCodes(financeAccountList : FinancialCode[]){
  this.financialCodeList = financeAccountList;
  if (this.financeAccountList?.length > 0) {
    // this.financialCode?.setValidators([
    //   Validators.required,
    //   autocompleteValidator(this.financialCodes),
    // ]);
    this.setFinancialCodeList();
  }
}

setFinancialCodeList() {
  if (this.financialCode) {
    this.filteredFinancialCodeList = this.financialCode.valueChanges.pipe(
      startWith(''),
      debounceTime(200),
      map(
        (value) =>
          this.financialCodeList.filter((option: FinancialCode) =>
            option?.value
              ?.toLowerCase()
              .includes(value?.toString().toLowerCase())
        ) ?? ''
      )
    );
  }
}

paymentMethods!: PaymentMethod[];
filteredPaymentMethodList!: Observable<PaymentMethod[]>;

@Input()
set setPaymentMethods(paymentMethods : PaymentMethod[]){
  this.paymentMethods = paymentMethods;
  if(this.paymentMethods?.length > 0){
    // this.paymentMethod?.setValidators([
    //   Validators.required,
    //   autocompleteValidator(this.paymentMethods)
    // ]);
    this.setPaymentMethodList();
  }
}

setPaymentMethodList() {
  if (this.paymentMethod) {
    this.filteredPaymentMethodList = this.paymentMethod.valueChanges.pipe(
      startWith(''),
      debounceTime(200),
      map(
        (value) =>
          this.paymentMethods.filter((option: PaymentMethod) =>
            option.value
              ?.toLowerCase()
              .includes(value.toString().toLowerCase())
          ) ?? ''
      )
    );
  }
}


  @Input() 
  set setPaymentlistBatchFormData(batchFormData : any[]){
      this.paymentlistBatchFormData = batchFormData;
      
    setTimeout(()=>{
      console.log('this.paymentlistBatchFormData',this.paymentlistBatchFormData);
      if (this.form && this.paymentlistBatchFormData) {
        const paymentArray = this.form.get('payments') as FormArray;
        console.log(this.paymentStatusList);
        const paymentStatus : any = this.paymentStatusList.find(e => e.id == 'paid');
        console.log(paymentStatus);
        this.paymentlistBatchFormData.forEach((obj: any, index: any) => {
          const createGroup = this.createPaymentFormGroup();
          createGroup.patchValue({
            id: obj?.id,
            payeeName: obj?.payeeName,
            payeeType: obj?.payeeType,
            teamName: obj?.teamName,
            contractRefNumber: obj?.contractNumber,
            invoiceNumber : '',
            paymentStatus: paymentStatus,
            invoiceDate: new Date(),
            paymentDate: obj?.paymentDate && new Date(obj.paymentDate),
            paymentAmount: obj?.amount,
            paidAmount: obj?.amount
          });
          paymentArray.push(createGroup);
        });
      }
    },200);
      
  }

  @Input() 
  set setPaymentStatusList(paymentStatusList : PaymentStatus[]){
    this.paymentStatusList = paymentStatusList;
    if(this.paymentStatusList.length > 0){
      setTimeout(()=>{
        this.paymentlistBatchFormData.forEach((obj: any, index: any) => {
        this.setFilteredStatusList(index);
        });
      },300);
    }
  }

  constructor(private fb: FormBuilder,
    private readonly messageService: MessageService) {}

  ngOnInit(): void {
    console.log('init');
    this.initializeForm();
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('onChange');
  }

  initializeForm(resetForm?: boolean) {
    if (this.form && !resetForm) return;

    this.form = this.fb.group({
      cashbookType:[''],
      fundSource:[''],
      payerId:[''],
      financialCode:[''],
      paymentMethod:[''],
      refNo:[''],
      description:[''],
      payments: this.fb.array([])
    });
  }

  createPaymentFormGroup() {
    return this.fb.group({
      id:[''],
      payeeName: [''],
      payeeType: [''],
      teamName:[''],
      contractRefNumber:[''],
      invoiceNumber:[''],
      invoiceDate: [new Date()],
      paymentDate:[''],
      paymentStatus: [''],
      paymentAmount: [0, [Validators.required, Validators.pattern('^([০-৯,]|[0-9])+(.[0-9]*)?$')]],
      paidAmount: ['', [Validators.required, Validators.pattern('^([০-৯,]|[0-9])+(.[0-9]*)?$')]],
      vatAit: this.fb.array([])
    });
  }

  vatAitGroup(){
    return this.fb.group({ 
      vat: [0, [Validators.required, Validators.pattern('^([০-৯,]|[0-9])+(.[0-9]*)?$')]],
      vatAmount: [0, [Validators.required, Validators.pattern('^([০-৯,]|[0-9])+(.[0-9]*)?$')]],
      ait: [0, [Validators.required, Validators.pattern('^([০-৯,]|[0-9])+(.[0-9]*)?$')]],
      aitAmount: [0, [Validators.required, Validators.pattern('^([০-৯,]|[0-9])+(.[0-9]*)?$')]],
      netPayable: ['']
    });
    
  }

  isValidNumber(event: any, index: number) {
    const  paymentAmountField = this.paymentsArray.controls[index].get('paymentAmount');
    if (event) {
      const convertNum = this.convertToEnglishNum(event);
      paymentAmountField?.setValue(convertNum, {
        emitViewToModelChange: false,
      });
    }
  }

  setFilteredStatusList(index : number) {
    const  paymentStatusField = this.paymentsArray.controls[index].get('paymentStatus');
    this.filteredStatusList[index] = paymentStatusField?.valueChanges.pipe(
      startWith(''),
      debounceTime(200),
      map(
        (value) =>
          this.paymentStatusList?.filter((option: any) =>
            option?.value
              ?.toLowerCase()
              .includes(value.toString().toLowerCase())
          ) ?? ''
      )
    );
  }

  convertToEnglishNum(banglaString: string): string {
    return banglaString
      ?.toString()
      .replaceAll(/০/g, '0')
      .replace(/১/g, '1')
      .replace(/২/g, '2')
      .replace(/৩/g, '3')
      .replace(/৪/g, '4')
      .replace(/৫/g, '5')
      .replace(/৬/g, '6')
      .replace(/৭/g, '7')
      .replace(/৮/g, '8')
      .replace(/৯/g, '9')
      .replace(/[,]/g, '');
  }

  setAmountCalculation(index: number,event?: any,) {
    const vatAitArray = this.paymentsArray.controls[index].get('vatAit') as FormArray;
    const firstVatAitGroup =  vatAitArray?.at(0) as FormGroup;
    const  paidAmountField = this.paymentsArray.controls[index].get('paidAmount');
    const  paymentAmountField = this.paymentsArray.controls[index].get('paymentAmount');
    const  paymentStatusField = this.paymentsArray.controls[index].get('paymentStatus');

    if (event) {
      const convertNum = this.convertToEnglishNum(event);
      paidAmountField?.setValue(convertNum, {emitViewToModelChange: false});
    }
   
    paidAmountField?.valueChanges.pipe(
      debounceTime(100) 
    ).subscribe((val:any) => {
      if (
        val >= paymentAmountField?.value &&
        paymentStatusField?.value.id === 'partial_payment'
      ) {
        if (paidAmountField) {
          paidAmountField?.setValue(0);
          this.messageService.showErrorMessage(
            'Paid Amount must be less than the payment amount.'
          );
        }
      }
    });
    
    const amount = + paidAmountField?.value;
    const vat = + firstVatAitGroup?.get('vat')?.value;
    const ait = + firstVatAitGroup?.get('ait')?.value;
    const vatAmount = ((amount * vat) / 100).toFixed(2);
    const aitAmount = ((amount * ait) / 100).toFixed(2);
    const netPayabale = (Number(amount) - (Number(vatAmount) + Number(aitAmount))).toFixed(2);

    firstVatAitGroup?.get('vatAmount')?.setValue(vatAmount.toString(), {emitViewToModelChange: false});
    firstVatAitGroup?.get('aitAmount')?.setValue(aitAmount.toString(), {emitViewToModelChange: false});
    firstVatAitGroup?.get('netPayable')?.setValue(netPayabale.toString(), {emitViewToModelChange: false});
  }

  toggle(event: MatSlideToggleChange, index: number) {
    const vatAitArray = this.paymentsArray.controls[index].get('vatAit') as FormArray;
    const firstVatAitGroup =  vatAitArray.at(0) as FormGroup;
    this.toggleChecker = event.checked;

    
    if (event.checked && this.vatAitFormArray(index).length == 0) {
      this.vatAitFormArray(index).push(
        this.fb.group({
          vat: 15, 
          vatAmount: 0,
          isVatOrAit: event.checked,
          ait: 10,
          aitAmount: 0,
          netPayable: ''
        })
      );

      // vatField?.setValidators([
      //   Validators.required,
      //   Validators.pattern('^([০-৯,]|[0-9])+(.[0-9]*)?$'),
      // ]);
      // aitField?.setValidators([
      //   Validators.required,
      //   Validators.pattern('^([০-৯,]|[0-9])+(.[0-9]*)?$'),
      // ]);
    } else {
      this.vatAitFormArray(index).removeAt(0);
      // firstVatAitGroup?.get('vat')?.clearValidators();
      // firstVatAitGroup?.get('ait')?.clearValidators();
    }
      this.setAmountCalculation(index);
      // firstVatAitGroup?.get('vat')?.updateValueAndValidity();
      // firstVatAitGroup?.get('ait')?.updateValueAndValidity();
      console.log('this.vatAitFormArray(index).length::',this.vatAitFormArray(index).length);
  }

  convertToEnglish($event: any, index: number) {
    const vatAitArray = this.paymentsArray.controls[index].get('vatAit') as FormArray;
    const firstVatAitGroup =  vatAitArray.at(0) as FormGroup;

    const  vatField = firstVatAitGroup?.get('vat');
    const  aitField = firstVatAitGroup?.get('ait');
    const  vatAmountField = firstVatAitGroup?.get('vatAmount');
    const  aitAmountField = firstVatAitGroup?.get('aitAmount');
    if ($event) {
      if ($event === vatField?.value) {
        const _convertedNumber = this.convertToEnglishNum($event);
        vatField?.setValue(_convertedNumber,{emitViewToModelChange:false});
      } else if ($event === vatAmountField?.value) {
        const _convertedNumber = this.convertToEnglishNum($event);
        vatAmountField?.setValue(_convertedNumber,{emitViewToModelChange:false});
      } else if ($event === aitField?.value) {
        const _convertedNumber = this.convertToEnglishNum($event);
        aitField?.setValue(_convertedNumber,{emitViewToModelChange:false});
      } else if ($event === aitAmountField?.value) {
        const _convertedNumber = this.convertToEnglishNum($event);
        aitAmountField?.setValue(_convertedNumber,{emitViewToModelChange:false});
      }
    }
  }

  vatAitAmountCalculation(index : number) {
    const vatAitArray = this.paymentsArray.controls[index].get('vatAit') as FormArray;
    const firstVatAitGroup =  vatAitArray.at(0) as FormGroup;

    const  paidAmountField = this.paymentsArray.controls[index].get('paidAmount');
    const paidAmount = + paidAmountField?.value;
    const vatAmountInput = firstVatAitGroup?.get('vatAmount')?.value;
    const vatPercentage = ((vatAmountInput / paidAmount) * 100).toFixed(2);
    
    firstVatAitGroup?.get('vat')?.setValue(vatPercentage, {emitViewToModelChange: false});
    const aitAmountInput = firstVatAitGroup?.get('aitAmount')?.value;
    const aitPercentage = ((aitAmountInput / paidAmount) * 100).toFixed(2);
    firstVatAitGroup?.get('ait')?.setValue(aitPercentage, {emitViewToModelChange: false});
    const billedAmount =
      (Number(paidAmount) - (Number(vatAmountInput) + Number(aitAmountInput))).toFixed(2);
      firstVatAitGroup?.get('netPayable')?.setValue(billedAmount, {emitViewToModelChange: false});
  }

  setAmount(event:any, index: number) {
    const vatAitArray = this.paymentsArray.controls[index].get('vatAit') as FormArray;
    const firstVatAitGroup =  vatAitArray.at(0) as FormGroup;
    const paidAmountField = this.paymentsArray.controls[index].get('paidAmount');
  
    if (event?.id == 'partial_payment') {
      // paidAmountField?.enable();
      firstVatAitGroup?.get('vat')?.setValue(0);
      firstVatAitGroup?.get('ait')?.setValue(0);
      firstVatAitGroup?.get('netPayable')?.setValue(0);
      paidAmountField?.setValue(0);
      this.setAmountCalculation(index);
    } else {
      firstVatAitGroup?.get('vat')?.setValue(15);
      firstVatAitGroup?.get('ait')?.setValue(10);
      const  paymentAmountField = this.paymentsArray.controls[index].get('paymentAmount');
      paidAmountField?.setValue(Number(paymentAmountField?.value) ?? 0);
      this.setAmountCalculation(index);
      // paidAmountField?.disable();
    }
  }

  setCashbookTypeList() {
    if (this.cashbookType) {
      this.filteredCashbookTypeList = this.cashbookType.valueChanges.pipe(
        startWith(''),
        debounceTime(200),
        map(
          (value) =>
            this.cashbookTypeList?.filter((option: CashbookType) =>
              option?.value
                ?.toLowerCase()
                .includes(value.toString().toLowerCase())
          ) ?? ''
        )
      );
    }
  }

  setFundSourceTypeList(){
    if (this.fundSource) {
      this.filteredFundSourceTypeList = this.fundSource.valueChanges.pipe(
        startWith(''),
        debounceTime(200),
        map(
          (value) =>
          this.fundSourceTypeList?.filter((option: FundSource) =>
              option?.value
                ?.toLowerCase()
                .includes(value.toString().toLowerCase())
            ) ?? ''
        )
      );
    }
  }

  getSelectedFundSource(event : any){
    this.selectedFundFouceIdEmiter.emit(event.option.value?.id);
    this.payerId?.setValue('');
    this.financialCode?.setValue('');
  }

  displayAutocompleteValue(e: any): string {
    return e ? e.value : '';
  }

  get paymentsArray(): FormArray {
    return this.form.get('payments') as FormArray;
  }

  vatAitFormArray(index: number) : FormArray{
     return this.paymentsArray.at(index).get('vatAit') as FormArray;
  }

  convertToEn(banglaString: string) {
    const convertNum = this.convertToEnglishNum(banglaString);
    this.refNo?.setValue(convertNum, {emitViewToModelChange: false});
  }

  submit() {
    console.log('submit::',this.form.value);
    let invoiceDate :any;
    let paymentDate : any;
    const outputData : any= {
      cashbookEntry: this.form.value.cashbookType.id,
      payerType: "account",
      payerId: this.form.value.payerId.id,
      financialCodeId: this.form.value.financialCode.id,
      fundSourceId: this.form.value.fundSource.id,
      paymentMethodId: this.form.value.paymentMethod.id,
      reference: this.form.value.refNo,
      description: this.form.value.description,
      schedulerPaymentBeanList: [],
  };

  this.form.value.payments.forEach((payment : any, index:any) => {
    if (payment?.invoiceDate) {
      invoiceDate = new Date(payment?.invoiceDate ?? '');
      invoiceDate.setHours(6); 
      invoiceDate = invoiceDate.toISOString().slice(0, 10) ?? '';
    }

    if (payment?.paymentDate) {
      paymentDate = new Date(payment?.paymentDate ?? '');
      paymentDate.setHours(6); 
      paymentDate = paymentDate.toISOString().slice(0, 10) ?? '';
    }
      const schedulerPayment : any = {
          id: payment.id,
          payeeName: payment.payeeName,
          paidAmount: payment.paidAmount,
          paymentAmount: payment.paymentAmount,
          billedAmount: payment?.vatAit[index]?.netPayable ? payment?.vatAit[index]?.netPayable : 0,
          paymentStatus: payment.paymentStatus.id,
          paymentDate: paymentDate,
          invoiceNo: payment.invoiceNumber ?? "",
          invoiceDate: invoiceDate,
          vat: payment?.vatAit[index]?.vat ? payment?.vatAit[index]?.vat : 0,
          vatAmount: payment?.vatAit[index]?.vatAmount ? payment?.vatAit[index]?.vatAmount : 0,
          ait: payment?.vatAit[index]?.ait ? payment?.vatAit[index]?.ait : 0,
          aitAmount: payment?.vatAit[index]?.aitAmount ? payment?.vatAit[index]?.aitAmount : 0,
          isVatOrAit: payment?.vatAit[index]?.isVatOrAit ? payment?.vatAit[index]?.isVatOrAit : false,
      };
      outputData.schedulerPaymentBeanList.push(schedulerPayment);
  });

  console.log('outputData:::',outputData);
  this.onSubmit.emit(outputData);
  
  }

  get cashbookType() {
    return this.form.get('cashbookType');
  }

  get fundSource() {
    return this.form.get('fundSource');
  }
  
  get payerType(){
    return this.form.get('payerType');
  }
   
  get payerId(){
    return this.form.get('payerId');
  }

  get financialCode(){
    return this.form.get('financialCode')
  }

  get refNo() {
    return this.form.get('refNo');
  }

  get paymentMethod(){
    return this.form.get('paymentMethod');
  }

  get description(){
    return this.form.get('description');
  }

}
