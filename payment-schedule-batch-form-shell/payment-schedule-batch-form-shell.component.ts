import { Component, Input, OnChanges, SimpleChanges, signal } from '@angular/core';
import { FinanceAccount } from 'src/app/report/models/finance-account.model';
import { AllService } from 'src/app/report/services/all.service';
import { PaymentMethod } from '../models/payment-method.model';
import { FinanceService } from '../services/finance.service';
import { PaymentStatus } from '../models/payment-status.model';
import { CashbookType } from '../models/cashbook-type.model';
import { MessageService } from 'src/app/shared/services/message.service';
import { MatDialogRef } from '@angular/material/dialog';
import { PaymentScheduleListDialogComponent } from '../payment-schedule-list-dialog/payment-schedule-list-dialog.component';
import { FinancialCode } from '../models/financial-code.model';
import { FundSource } from '../models/fundsource.model';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-payment-schedule-batch-form-shell',
  templateUrl: './payment-schedule-batch-form-shell.component.html',
  styleUrls: ['./payment-schedule-batch-form-shell.component.css']
})
export class PaymentScheduleBatchFormShellComponent implements OnChanges {

  paymentlistBatchFormData = signal<any[]>([]);

  paymentStatusList = signal<PaymentStatus[]>([]);
  financeAccountList = signal<FinanceAccount[]>([]);
  financialCodes = signal<FinancialCode[]>([]);
  paymentMethods = signal<PaymentMethod[]>([]);
  cashbookTypes = signal<CashbookType[]>([]);
  fundSourceTypes = signal<FundSource[]>([]);


  @Input() paymentSchedulListDialogObj: any;

  constructor(private allService: AllService,
    private readonly financeService: FinanceService,
    private readonly messageService: MessageService,
    private dialogRef: MatDialogRef<PaymentScheduleListDialogComponent>
  ) { }
  
  
  ngOnChanges(changes: SimpleChanges): void {
    console.log('this.paymentSchedulListDialogObj-shell', this.paymentSchedulListDialogObj?.paymentBean);
    this.paymentlistBatchFormData.set(this.paymentSchedulListDialogObj?.paymentBean);
  }

  ngOnInit() {
    this.getCashbookTypes();
    this.getFundSources();
    this.getPaymentStatusList();
    this.getPaymentMethods();
  }
  getFundSources() {
    this.financeService.getFundSources().subscribe(fundSources => {
      this.fundSourceTypes.set(fundSources)
    },error=>{
      this.cashbookTypes.set([]);
    })
  }

  getPaymentMethods() {
    this.financeService.getPaymentMethods().subscribe(paymentMethods => {
      this.paymentMethods.set(paymentMethods)
    }, error => {
      this.paymentMethods.set([])
    })
  }

  getPaymentStatusList() {
    this.allService.getStatus().subscribe((data: any) => {
      this.paymentStatusList.set(data);
    },error => {
      this.paymentStatusList.set([]);
    })
  }

  getCashbookTypes() {
    this.financeService.getCashbookTypes().subscribe(cashbookTypes => {
      this.cashbookTypes.set(cashbookTypes)
    },error=>{
      this.cashbookTypes.set([]);
    })
  }

  getSelectedFundFouceId(id: any) {
    const chargeAccount = this.allService.getChargedAccount(id);
    const financeCode = this.financeService.getFinanceCodesByExpense(id);
    
    forkJoin([chargeAccount,financeCode]).subscribe({
      next : (response) => {
        this.financeAccountList.set(response[0]);
        this.financialCodes.set(response[1]);
      },
      error: (error) => {
        this.financeAccountList.set([]);
        this.financialCodes.set([]);
      }
    })
  }

  submit(paymentData : any){
    this.financeService.batchPaymentSubmit(paymentData).subscribe(res => {
      if(res){
      this.messageService.showSuccessMessage('Make Batch Payment Successfully');
      this.dialogRef.close(res);
      }  
    },error=>{
      this.messageService.showErrorMessage(error);
    });

  }

}
